#!/bin/sh

docker exec -i newsletter-pg psql -U postgres -c "drop database if exists newsletter" &&
docker exec -i newsletter-pg psql -U postgres -c "create database newsletter"

