# Golang Newsletter (Backend)
## Mock Email Service By Postman mock service
### Separated Commamds for Run Development Server Locally


1. Start PostgreSQL docker

```sh
./tools/start_postgresql_docker.sh
```

2. Reset Database. Your previous data will be lost.

```sh
./tools/reset_db.sh
```

3. Migrate Table.

```sh
./tools/migrate.sh
```

4. Start HTTP Server using `config.yaml` as a configuration.
```sh
./tools/serve.sh
```

## Script send newsletter weekly

1. cd src go run main.go send_newsletter  --config=config/config.yaml `or`  
```sh
./tools/send_newsletter_weekly.sh
```

### API CURL Local


#### api subscriber
curl --location 'localhost:3000/api/v1/subscriber' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "junber_1@gmail.com",
    "category_code": "NEWS_001"
}'


#### api un-subscriber
curl --location 'localhost:3000/api/v1/un-subscriber' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email" :"junber_1@gmail.com",
    "category_code":"NEWS_001"
}'

#### api category-list

curl --location 'localhost:3000/api/v1/category-list'
#### Provide PostMan Collecttion
file_name: docs/Newsletter.postman_collection.json

#### Flow Chart Subscriber 
![plot](./docs/flow_chart_sub.png)