package service

import (
	"newsletter/logger"

	"go.uber.org/zap"
)

type Service struct {
	RequestId string
	Logger    *zap.SugaredLogger
}

func New(requestId string) Service {
	serviceObj := Service{
		RequestId: requestId,
	}

	serviceObj.Logger = logger.Logger.With(
		"request_id", requestId,
		"part", "service",
	)

	return serviceObj
}
