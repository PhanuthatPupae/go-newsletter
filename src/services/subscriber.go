package service

import (
	"newsletter/custom_error"
	"newsletter/db/postgresql"
	"newsletter/model"
	"newsletter/repository"
)

type SubscriberInput struct {
	Email        string `json:"email" binding:"required"`
	CategoryCode string `json:"category_code" binding:"required"`
}

type SubscriberOutput struct {
	SubscriberID uint `json:"subscriber_id"`
}

func (service Service) Subscriber(input SubscriberInput) (SubscriberOutput, error) {
	service.Logger.Infof("start Subscriber")
	output := SubscriberOutput{}

	repo := repository.New(service.RequestId)
	tx := postgresql.GormDB.Begin()
	if err := tx.Error; err != nil {
		service.Logger.Errorf("begin tx fail error: %s", err.Error())
		return output, err
	}
	defer tx.Rollback()

	category, err := repo.GetCategoryByCondition(
		model.Category{
			Code: input.CategoryCode,
		})
	if err != nil {
		service.Logger.Errorf("GetCategoryByCondition fail error:%s", err.Error())
		return output, err
	}

	exits, err := repo.GetSubscriberCategoryExist(
		tx,
		input.Email,
		category.ID,
	)

	if err != nil {
		service.Logger.Errorf("GetSubscriberCategoryExist fail error:%s", err.Error())
		return output, err
	}

	if exits {
		service.Logger.Info(custom_error.SubscriberConflictString)
		return output, custom_error.CustomError{
			Code:    custom_error.SubscriberConflict,
			Message: custom_error.SubscriberConflictString,
		}

	}

	subscriberNew, err := repo.CreateSubscriber(
		tx,
		model.Subscriber{
			Email: input.Email,
		})

	if err != nil {
		service.Logger.Errorf("CreateSubscriber fail error:%s", err.Error())
		return output, err
	}

	_, err = repo.CreateSubscriberCategory(
		tx,
		model.SubscriberCategory{
			SubscriberID: subscriberNew.ID,
			CategoryID:   category.ID,
		})
	if err != nil {
		service.Logger.Errorf("CreateSubscriberCategory fail error:%s", err.Error())
		return output, err
	}

	if err := tx.Commit().Error; err != nil {
		service.Logger.Errorf("tx commit fail error: %s", err.Error())
		return output, err
	}

	service.Logger.Infof("Subscriber Done")
	output.SubscriberID = subscriberNew.ID
	return output, nil
}

type UnSubscriberInput struct {
	Email        string `json:"email" binding:"required"`
	CategoryCode string `json:"category_code" binding:"required"`
}

type UnSubscriberOutput struct {
}

func (service Service) UnSubscriber(input UnSubscriberInput) (UnSubscriberOutput, error) {
	service.Logger.Infof("start UnSubscriber")
	output := UnSubscriberOutput{}

	repo := repository.New(service.RequestId)
	tx := postgresql.GormDB.Begin()
	if err := tx.Error; err != nil {
		service.Logger.Errorf("begin tx fail error: %s", err.Error())
		return output, err
	}
	defer tx.Rollback()

	category, err := repo.GetCategoryByCondition(
		model.Category{
			Code: input.CategoryCode,
		})
	if err != nil {
		service.Logger.Errorf("GetCategoryByCondition fail error:%s", err.Error())
		return output, err
	}

	exits, err := repo.GetSubscriberCategoryExist(
		tx,
		input.Email,
		category.ID,
	)

	if err != nil {
		service.Logger.Errorf("GetSubscriberCategoryExist fail error:%s", err.Error())
		return output, err
	}

	service.Logger.Infof("email:%s category_code:%s", input.Email, category.Code)
	if exits {
		subscriber, err := repo.GetSubscriber(
			tx,
			model.Subscriber{
				Email: input.Email,
			})
		if err != nil {
			service.Logger.Errorf("GetSubscriber fail error:%s", err.Error())
			return output, err
		}

		service.Logger.Infof("GetSubscriber subscriber_id:%v", subscriber.ID)

		_, err = repo.UnSubscriberCategory(
			tx,
			model.SubscriberCategory{
				SubscriberID: subscriber.ID,
				CategoryID:   category.ID,
			})
		if err != nil {
			service.Logger.Errorf("UnSubscriberCategory fail error:%s", err.Error())
			return output, err
		}

		service.Logger.Infof("UnSubscriberCategory Done")

		subscriberCategories, err := repo.GetSubscriberCategoryList(
			tx,
			model.SubscriberCategory{
				SubscriberID: subscriber.ID,
			})
		if err != nil {
			service.Logger.Errorf("GetSubscriberCategoryList fail error:%s", err.Error())
			return output, err
		}

		if len(subscriberCategories) == 0 {
			err = repo.Unsubscribe(tx, model.Subscriber{ID: subscriber.ID})
			if err != nil {
				service.Logger.Errorf("Unsubscribe fail error:%s", err.Error())
				return output, err
			}
		}

	}

	if err := tx.Commit().Error; err != nil {
		service.Logger.Errorf("tx commit fail error: %s", err.Error())
		return output, err
	}

	service.Logger.Infof("UnSubscriber Done")
	return output, nil
}
