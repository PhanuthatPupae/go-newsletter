package service

import (
	"context"

	"newsletter/external/mail"
	"newsletter/repository"
)

type SendNewsLetterWeeklyInput struct {
}

type SendNewsLetterWeeklyOutput struct {
}

func (service Service) SendNewsLetterWeekly(input SendNewsLetterWeeklyInput) (SendNewsLetterWeeklyOutput, error) {
	service.Logger.Infof("start SendNewsLetterWeekly")
	output := SendNewsLetterWeeklyOutput{}

	emailService := mail.New(context.Background(), service.RequestId)
	repo := repository.New(service.RequestId)
	category, err := repo.GetCategoryList()
	if err != nil {
		service.Logger.Infof("GetCategoryList fail error:%s", err.Error())
		return output, err
	}

	for _, c := range category {
		newletter, err := repo.GetNewslettersPublishedThisWeekWithDetails(c.Code)
		if err != nil {
			service.Logger.Infof("GetNewslettersPublishedThisWeekWithDetails fail error:%s", err.Error())
			return output, err
		}
		for _, n := range newletter {
			_, err := emailService.SendEmail(mail.SendMailInput{
				SendTo:  n.Email,
				Title:   n.Title,
				Content: n.Content,
			})
			if err != nil {
				service.Logger.Infof("SendEmail fail error:%s", err.Error())
				return output, err
			}
		}

	}

	service.Logger.Infof("SendNewsLetterWeekly Done")
	return output, nil
}
