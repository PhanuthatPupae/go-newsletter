package service

import (
	"newsletter/model"
	"newsletter/repository"
)

func (service Service) CategoryList() ([]model.Category, error) {
	service.Logger.Infof("start CategoryList")

	output := []model.Category{}

	repo := repository.New(service.RequestId)
	categories, err := repo.GetCategoryList()
	if err != nil {
		service.Logger.Infof("GetCategoryList fail error %s", err.Error())
		return output, err
	}

	service.Logger.Infof("CategoryList Done")
	output = categories
	return output, nil

}
