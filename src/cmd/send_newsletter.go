package cmd

import (
	"github.com/google/uuid"
	"github.com/spf13/cobra"

	postgresql "newsletter/db/postgresql"
	"newsletter/logger"
	service "newsletter/services"
)

var SendNewsletter = &cobra.Command{
	Use:   "send_newsletter",
	Short: "send_newsletter",
	RunE: func(cmd *cobra.Command, args []string) error {
		uuid := uuid.New().String()
		logger.InitLogger()
		postgresql.InitGormDB(false)

		s := service.New(uuid)

		_, err := s.SendNewsLetterWeekly(service.SendNewsLetterWeeklyInput{})
		if err != nil {
			s.Logger.Errorf("SendNewsLetterWeekly failed cuz: %v", err)
			return err
		}

		s.Logger.Errorf("SendNewsLetterWeekly Done")

		return nil

	},
}

func init() {
	rootCmd.AddCommand(SendNewsletter)

}
