package mail

import (
	"context"
	"encoding/json"
	"net/http"
)

type SendMailInput struct {
	SendTo  string
	Title   string
	Content string
}

type SendMailOutput struct {
	Code    string
	Message string
}

func (mail *Mail) SendEmail(input SendMailInput) (*SendMailOutput, error) {

	body, err := json.Marshal(input)
	if err != nil {
		return nil, err
	}

	url := mail.UrlEmail
	status, _, res, err := mail.RequestHttp(context.Background(), url, body, "POST", "")
	if status != http.StatusOK {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	output := SendMailOutput{}
	err = json.Unmarshal(res, &output)
	if err != nil {
		return nil, err
	}
	return &output, nil
}
