package model

import (
	"time"

	"gorm.io/gorm"
)

type SubscriberTransactionLog struct {
	ID        uint           `gorm:"primary_key" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`

	SubscriberID uint                   `json:"subscriber_id"`
	CategoryID   uint                   `json:"category_id"`
	Action       string                 `json:"action"`
	OldData      map[string]interface{} `json:"old_data" gorm:"type:jsonb"`
	NewData      map[string]interface{} `json:"new_data" gorm:"type:jsonb"`
}
