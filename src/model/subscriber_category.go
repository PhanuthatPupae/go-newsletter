package model

import (
	"time"

	"gorm.io/gorm"
)

type SubscriberCategory struct {
	ID        uint           `gorm:"primary_key" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`

	SubscriberID uint
	CategoryID   uint
	SubscribedAt time.Time

	Category   Category   `gorm:"foreignkey:CategoryID"`
	Subscriber Subscriber `gorm:"foreignkey:SubscriberID"`
}
