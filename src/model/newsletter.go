package model

import (
	"time"

	"gorm.io/gorm"
)

type Newsletter struct {
	ID        uint           `gorm:"primary_key" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`

	Title        string `json:"title"`
	Content      string `json:"content"`
	CategoryCode string `json:"category_code"`

	PublicationDate time.Time
}

type NewsLetterWeekly struct {
	Title        string `json:"title"`
	Content      string `json:"content"`
	CategoryCode string `json:"category_code"`
	Email        string
}
