package custom_error

const errorCodeBase = 0

const (
	// original codes
	UnknownError         int32 = errorCodeBase + 1
	InvalidJSONString    int32 = errorCodeBase + 2
	InputValidationError int32 = errorCodeBase + 3
	BadRequestError      int32 = errorCodeBase + 4
	InvalidMultiplePath  int32 = errorCodeBase + 2

	// proprietary codes
	DatabaseError int32 = errorCodeBase + 8

	BadRequest       int32  = 400
	BadRequestString string = "bad request"
	NoRequestString  string = "no request in body"

	DataNotFound       int32  = 404
	DataNotFoundString string = "data not found"

	Unauthorize       int32  = 401
	UnauthorizeString string = "unauthorize"

	InternalServerError       int32  = 500
	InternalServerErrorString string = "internal server error"

	SubscriberConflict       int32  = 409
	SubscriberConflictString string = "subscriber already exist"
)
