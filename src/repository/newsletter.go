package repository

import (
	"time"

	"newsletter/db/postgresql"
	"newsletter/model"
)

func (repo Repository) CreateNewletter(newletter model.Newsletter) (model.Newsletter, error) {
	err := postgresql.GormDB.Model(&model.Newsletter{}).Create(&newletter).Error
	if err != nil {
		return newletter, err
	}
	return newletter, nil
}

func (repo Repository) GetNewletterByCategory(newletter model.Newsletter) ([]model.Newsletter, error) {
	result := []model.Newsletter{}
	err := postgresql.GormDB.Model(&model.Newsletter{}).Where("category_code = ?", newletter.CategoryCode).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) GetNewslettersPublishedThisWeekWithDetails(categoryCode string) ([]model.NewsLetterWeekly, error) {
	result := []model.NewsLetterWeekly{}

	location, err := time.LoadLocation(repo.TimeZone)
	if err != nil {
		return result, err
	}

	startOfWeek := time.Now().In(location).Truncate(24 * time.Hour).Add(-time.Duration(time.Now().Weekday()-1) * 24 * time.Hour)
	endOfWeek := startOfWeek.Add(7 * 24 * time.Hour)

	err = postgresql.GormDB.
		Table("newsletters").
		Select("newsletters.title, newsletters.content, newsletters.category_code, subscribers.email AS email").
		Joins("LEFT JOIN categories ON newsletters.category_code = categories.code").
		Joins("LEFT JOIN subscriber_categories ON categories.id = subscriber_categories.category_id").
		Joins("LEFT JOIN subscribers ON subscriber_categories.subscriber_id = subscribers.id").
		Where("newsletters.publication_date BETWEEN ? AND ?", startOfWeek, endOfWeek).
		Where("newsletters.category_code = ?", categoryCode).
		Where("newsletters.deleted_at is null").
		Where("subscribers.deleted_at is null").
		Where("subscriber_categories.deleted_at is null").
		Where("categories.deleted_at is null").
		Find(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil

}
