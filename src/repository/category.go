package repository

import (
	"newsletter/db/postgresql"
	"newsletter/model"
)

func (repo Repository) CreateCategory(category model.Category) (model.Category, error) {
	err := postgresql.GormDB.Model(&model.Category{}).Create(&category).Error
	if err != nil {
		return category, err
	}
	return category, nil
}

func (repo Repository) GetCategoryByCondition(category model.Category) (model.Category, error) {
	result := model.Category{}
	err := postgresql.GormDB.Model(&model.Category{}).Where(category).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) GetCategoryList() ([]model.Category, error) {
	result := []model.Category{}
	err := postgresql.GormDB.Model(&model.Category{}).Find(&result).Error
	if err != nil {
		return result, err
	}
	return result, nil
}
