package repository

import (
	"time"

	"gorm.io/gorm"

	"newsletter/db/postgresql"
	"newsletter/model"
)

func (repo Repository) CreateSubscriber(db *gorm.DB, subscriber model.Subscriber) (model.Subscriber, error) {

	if db == nil {
		db = postgresql.GormDB
	}

	if err := db.FirstOrCreate(&subscriber).Error; err != nil {
		return subscriber, err
	}
	return subscriber, nil
}

func (repo Repository) Unsubscribe(db *gorm.DB, subscriber model.Subscriber) error {

	if db == nil {
		db = postgresql.GormDB
	}

	if err := db.
		Model(model.Subscriber{}).
		Where(model.Subscriber{
			ID: subscriber.ID,
		}).
		Delete(&subscriber).Error; err != nil {
		return err
	}

	return nil
}

func (repo Repository) GetSubscriber(db *gorm.DB, subscriber model.Subscriber) (model.Subscriber, error) {

	if db == nil {
		db = postgresql.GormDB
	}

	result := model.Subscriber{}

	if err := db.Model(model.Subscriber{}).Find(&result).Error; err != nil {
		return result, err
	}

	return result, nil
}

func (repo Repository) CreateSubscriberCategory(db *gorm.DB, subscriberCategory model.SubscriberCategory) (model.SubscriberCategory, error) {

	if db == nil {
		db = postgresql.GormDB
	}

	subscriberCategory.SubscribedAt = time.Now()

	if err := db.Create(&subscriberCategory).Error; err != nil {
		return subscriberCategory, err
	}
	return subscriberCategory, nil
}

func (repo Repository) UnSubscriberCategory(db *gorm.DB, subscriberCategory model.SubscriberCategory) (model.SubscriberCategory, error) {

	if db == nil {
		db = postgresql.GormDB
	}

	if err := db.Debug().
		Model(model.SubscriberCategory{}).
		Where("subscriber_id = ? and category_id = ? ", subscriberCategory.SubscriberID, subscriberCategory.CategoryID).
		Delete(&subscriberCategory).Error; err != nil {
		return subscriberCategory, err
	}
	return subscriberCategory, nil
}

func (repo Repository) GetSubscriberCategory(db *gorm.DB, subscriberCategory model.SubscriberCategory) (model.SubscriberCategory, error) {

	if db == nil {
		db = postgresql.GormDB
	}

	result := model.SubscriberCategory{}

	if err := db.Model(&model.SubscriberCategory{}).Where(subscriberCategory).Find(&result).Error; err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) GetSubscriberCategoryList(db *gorm.DB, subscriberCategory model.SubscriberCategory) ([]model.SubscriberCategory, error) {

	if db == nil {
		db = postgresql.GormDB
	}

	result := []model.SubscriberCategory{}

	if err := db.Model(&model.SubscriberCategory{}).Where(subscriberCategory).Find(&result).Error; err != nil {
		return result, err
	}
	return result, nil
}

func (repo Repository) GetSubscriberCategoryExist(db *gorm.DB, email string, categoryID uint) (bool, error) {
	if db == nil {
		db = postgresql.GormDB
	}

	query := `
        SELECT EXISTS (
            SELECT 1
            FROM subscribers
            WHERE email = ? AND deleted_at is null AND EXISTS (
                SELECT 1
                FROM subscriber_categories
                WHERE subscriber_id = subscribers.id AND category_id = ? AND deleted_at is null
            )
        )`

	var result bool
	if err := db.Raw(query, email, categoryID).Row().Scan(&result); err != nil {
		return false, err
	}

	return result, nil
}
