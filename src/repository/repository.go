package repository

import (
	"github.com/spf13/viper"
	"go.uber.org/zap"

	"newsletter/logger"
)

type Repository struct {
	RequestId string
	Logger    *zap.SugaredLogger
	TimeZone  string
}

func New(requestId string) Repository {
	modelObj := Repository{
		RequestId: requestId,
	}
	modelObj.Logger = logger.Logger.With(
		"request_id", requestId,
		"part", "repository",
	)
	modelObj.TimeZone = viper.GetString("System.TimeZone")
	return modelObj
}
