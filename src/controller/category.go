package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"newsletter/interface/http/response"
	service "newsletter/services"
)

func CategoryList(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to category list")
	responseOutput := response.ResponseOutput{}

	requestId, _ := context.Get("request_id")
	requestIdStr := requestId.(string)

	serviceObj := service.New(requestIdStr)
	responseData, err := serviceObj.CategoryList()
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = responseData
	context.JSON(http.StatusOK, responseOutput)
}
