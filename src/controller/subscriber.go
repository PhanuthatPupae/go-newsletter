package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"newsletter/interface/http/response"
	service "newsletter/services"
)

func Subscriber(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to subscriber")
	responseOutput := response.ResponseOutput{}

	requestId, _ := context.Get("request_id")
	requestIdStr := requestId.(string)

	input := service.SubscriberInput{}

	if err := context.ShouldBindJSON(&input); err != nil {
		apiLogger.Errorf("could not bind json body to subscriber for validation : %s", err.Error())
		responseOutput.Message = err.Error()
		context.JSON(http.StatusBadRequest, responseOutput)
		return
	}

	serviceObj := service.New(requestIdStr)
	responseData, err := serviceObj.Subscriber(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = responseData
	context.JSON(http.StatusOK, responseOutput)
}

func UnSubscriber(context *gin.Context) {
	contextLogger, _ := context.Get("logger")
	apiLogger := contextLogger.(*zap.SugaredLogger)
	apiLogger.Info("call interface to unsubscriber")
	responseOutput := response.ResponseOutput{}

	requestId, _ := context.Get("request_id")
	requestIdStr := requestId.(string)

	input := service.UnSubscriberInput{}

	if err := context.ShouldBindJSON(&input); err != nil {
		apiLogger.Errorf("could not bind json body to unsubscriber for validation : %s", err.Error())
		responseOutput.Message = err.Error()
		context.JSON(http.StatusBadRequest, responseOutput)
		return
	}

	serviceObj := service.New(requestIdStr)
	responseData, err := serviceObj.UnSubscriber(input)
	if err != nil {
		responseOutput, httpStatus := response.ResponseError(err)
		context.JSON(httpStatus, responseOutput)
		return
	}

	responseOutput.Message = "success"
	responseOutput.Data = responseData
	context.JSON(http.StatusOK, responseOutput)
}
