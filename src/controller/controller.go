package controller

import (
	"go.uber.org/zap"

	"newsletter/logger"
)

type Controller struct {
	RequestId string
	Logger    *zap.SugaredLogger
}

func New(requestId *string) Controller {
	controllerObj := Controller{
		RequestId: *requestId,
	}

	controllerObj.Logger = logger.Logger.With(
		"request_id", *requestId,
		"part", "controller",
	)

	return controllerObj
}
