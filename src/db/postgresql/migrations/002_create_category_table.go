package migration

import (
	"gorm.io/gorm"

	"newsletter/model"
)

var CreateCategoryTable = &Migration{
	Number: 2,
	Name:   "create category table",
	Forwards: func(db *gorm.DB) error {
		err := db.AutoMigrate(&model.Category{})
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateCategoryTable)
}
