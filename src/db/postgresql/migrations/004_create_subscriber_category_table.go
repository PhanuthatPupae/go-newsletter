package migration

import (
	"gorm.io/gorm"

	"newsletter/model"
)

var CreateSubscriberCategoryTable = &Migration{
	Number: 4,
	Name:   "create subscriber_category table",
	Forwards: func(db *gorm.DB) error {
		err := db.AutoMigrate(&model.SubscriberCategory{})
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateSubscriberCategoryTable)
}
