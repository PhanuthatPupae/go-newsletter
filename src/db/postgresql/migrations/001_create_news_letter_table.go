package migration

import (
	"gorm.io/gorm"

	"newsletter/model"
)

var CreateFileTable = &Migration{
	Number: 1,
	Name:   "create news_letter table",
	Forwards: func(db *gorm.DB) error {
		err := db.AutoMigrate(&model.Newsletter{})
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateFileTable)
}
