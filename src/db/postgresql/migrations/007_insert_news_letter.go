package migration

import (
	"gorm.io/gorm"
)

var InsertNewsLetter = &Migration{
	Number: 7,
	Name:   "insert newsletter",
	Forwards: func(db *gorm.DB) error {

		sql := `
		INSERT INTO newsletter.newsletters (created_at,updated_at,deleted_at,title,"content",category_code,publication_date) VALUES
		('now()','now()',NULL,'บัวลอย','บัวลอยไข่หวาน','NEWS_002','now()'),
		('now()','now()',NULL,'บอลไทย','บอลไทย','NEWS_001','now()');
   
   `
		err := db.Exec(sql).Error
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, InsertNewsLetter)
}
