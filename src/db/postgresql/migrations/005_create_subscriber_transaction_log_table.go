package migration

import (
	"gorm.io/gorm"

	"newsletter/model"
)

var CreateSubscriberTransactionLogTable = &Migration{
	Number: 5,
	Name:   "create subscriber_transaction_log table",
	Forwards: func(db *gorm.DB) error {
		err := db.AutoMigrate(&model.SubscriberTransactionLog{})
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateSubscriberTransactionLogTable)
}
