package migration

import (
	"gorm.io/gorm"

	"newsletter/model"
)

var CreateSubscriberTable = &Migration{
	Number: 3,
	Name:   "create subscriber table",
	Forwards: func(db *gorm.DB) error {
		err := db.AutoMigrate(&model.Subscriber{})
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, CreateSubscriberTable)
}
