package migration

import (
	"gorm.io/gorm"
)

var InsertCategory = &Migration{
	Number: 6,
	Name:   "insert category",
	Forwards: func(db *gorm.DB) error {

		sql := `
		INSERT INTO newsletter.categories (created_at,updated_at,name,code) VALUES
		('now()','now()','กีฬา','NEWS_001'),
		('now()','now()','ของกิน','NEWS_002');
   `
		err := db.Exec(sql).Error
		if err != nil {
			return err
		}

		return nil
	},
}

func init() {
	Migrations = append(Migrations, InsertCategory)
}
