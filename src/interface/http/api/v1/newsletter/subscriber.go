package newsletter

import (
	"newsletter/controller"
)

func init() {
	methodRoutes[ROUTE_POST]["/subscriber"] = controller.Subscriber
	methodRoutes[ROUTE_POST]["/un-subscriber"] = controller.UnSubscriber

}
